#include "TrkVertexWeightCalculators/SumPtVertexWeightCalculator.h"
#include "TrkVertexWeightCalculators/TrueVertexDistanceWeightCalculator.h"
#include "TrkVertexWeightCalculators/BDTVertexWeightCalculator.h"
#include "TrkVertexWeightCalculators/DecorateVertexScoreAlg.h"

using namespace Trk;

DECLARE_COMPONENT( SumPtVertexWeightCalculator )
DECLARE_COMPONENT( TrueVertexDistanceWeightCalculator )
DECLARE_COMPONENT( BDTVertexWeightCalculator )
DECLARE_COMPONENT( DecorateVertexScoreAlg )